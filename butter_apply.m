function butter_apply(data_path, subID, out_path)

fprintf('Processing: %s...\n', subID);

afni_funcs = 'path to afni MATLAB AFNI toolbox' % Edit with your path
addpath(genpath(afni_funcs));

Opt.Format = 'vector'; %reshape BRIK 4D into 2D
[err,brik,info] = BrikLoad(data_path ,Opt); % Load BRIK
brik_new = brik; % Create duplicate brik to manipulate
    
% SETUP FILTER PROPERTIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tr = 2.5;
f = 1/tr;
nyq = f/2; % Nyquist Frequency
lo = .009; hi = .08;
bWfilt = [lo hi];
bWorth = bWfilt / nyq;

bp_order = 2;

[b, a] = butter(bp_order/2, bWorth);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ii = 1 : length(brik(:, 1));
brik_new(ii, :) = filtfilt(b, a, brik(ii, :)); % Apply filter 

Opt.Prefix = out_path;
[err2, ErrM2, Info2] = WriteBrik(brik_new, info, Opt);

end
